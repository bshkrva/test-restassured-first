import data.schools.School;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static data.VkMethodsContants.*;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.core.IsEqual.equalTo;

public class VkApiDatabaseGetSchoolsMethodTest {
    private Map object;
    @Before
    public void setUp() {
        object = new HashMap();
        object.put(PARAM_SEARCH_QUERY, School.SCHOOL_QUERY.getValue());
        object.put(PARAM_CITY_ID, "4306");
        object.put(PARAM_VERSION,VERSION_VK_API);
        RestAssured.baseURI = VK_API_BASE_URI;
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

    }


    @Test
    public void test_VkApiGetSchoolsMethod_status_code_is_200() {

        given().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
        with().
                queryParams(object).
        when().
                post(VK_API_GET_SCHOOLS_METHOD).
        then().
                //log().all().
                statusCode(200);
    }

    @Test
    public void test_VkApiGetSchoolsMethod_status_code_is_200_and_error() {
        Map obj = new HashMap();
        object.put(PARAM_SEARCH_QUERY, School.SCHOOL_QUERY.getValue());
        object.put(PARAM_CITY_ID, "\"4306\"");
        object.put(PARAM_VERSION,VERSION_VK_API);
        given().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
        with().
                queryParams(object).
        when().
                post(VK_API_GET_SCHOOLS_METHOD).
        then().
               // log().all().
                statusCode(200).
                and().
                body("error.error_code", equalTo(100));

    }


    @Test
    public void test_VkApiGetSchoolsMethod_validation_json_scheme() {
        //надо положить json в правильную папочку:
       // System.out.println(this.getClass().getResource("/").getPath());
        given().
                contentType("application/json").
                accept("application/json").
        with().
                queryParams(object).
        when().
                post(VK_API_GET_SCHOOLS_METHOD).
        then().
                assertThat().
                body(matchesJsonSchemaInClasspath("school.json"));
    }
}
