package data.users;

public class UserTestingData {
    private static String USER_ID [] = {"210700286","41806379","32481524"};
    private static String FIRST_NAME[] = {"Lindsey","Сергей","Мария"};
    private static String LAST_NAME[] = {"Stirling","Гагин","Крывда"};

    static public String getUserID(int num){
        return (num < 3) ? USER_ID[num] : "";
    }

    static public String getFirstName(int num){
        return (num < 3) ? FIRST_NAME[num] : "";
    }

    static public String getLastName(int num){
        return (num < 3) ? LAST_NAME[num] : "";
    }
}
