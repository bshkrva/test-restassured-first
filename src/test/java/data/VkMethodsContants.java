package data;

public class VkMethodsContants {

    public static String VK_API_USER_GET_URI = "https://api.vk.com/method/users.get";
    public static String VK_API_BASE_URI = "https://api.vk.com/method";
    public static String VK_API_GET_USERS_METHOD = "users.get";
    public static String VK_API_GET_SCHOOLS_METHOD = "database.getSchools";
    public static String VK_API_GET_USERS_NEARBY_METHOD = "users.getNearby";

    public static String PARAM_USER_ID = "user_id";
    public static String PARAM_ACCESS_TOKEN = "access_token";
    public static String PARAM_SEARCH_QUERY = "q";
    public static String PARAM_CITY_ID = "city_id";
    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_COUNT = "count";

    public static String PARAM_SCHOOL_ID = "id";
    public static String PARAM_SCHOOL_TITLE = "title";

    public static String PARAM_LATITUDE = "latitude";
    public static String PARAM_LONGITUDE = "longitude";
    public static String PARAM_ACCURACY = "accuracy";
    public static String PARAM_RADIUS = "radius";

    public static String PARAM_VERSION = "v";
    public static String VERSION_VK_API = "5.69";

}
