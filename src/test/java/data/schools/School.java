package data.schools;

public enum School {
    SCHOOL_ID("60481"),
    SCHOOL_TITLE("Пригородная школа"),
    SCHOOL_QUERY("школа");
    private String value;

    School(String value){
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
