
import data.users.*;
import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static data.VkMethodsContants.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasItem;


public class VkApiUsersGetMethodTest {

    @Before
    public void setUp() {

        RestAssured.baseURI = VK_API_BASE_URI;
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    @Test
    public void test_VkApiGetUsers_status_code_is_200() {
        given ().
                param (PARAM_USER_ID, Integer.parseInt(UserTestingData.getUserID(0))).
                param (PARAM_VERSION, VERSION_VK_API).
        when().
                get(VK_API_GET_USERS_METHOD).
        then().
                assertThat().
                statusCode(200);
    }

    @Test
    public void test_VkApiGetUsers_response_has_expected_value() {
        given().
                param (PARAM_USER_ID, Integer.parseInt(UserTestingData.getUserID(1))).
                param (PARAM_VERSION, VERSION_VK_API).
        when().
                get(VK_API_GET_USERS_METHOD).
        then().
                log().all().
                assertThat().
                //в json лежит массив, поэтому простой equalTo не подходит
                body("response.first_name", hasItem(UserTestingData.getFirstName(1)));
    }

    @Test
    public void test_VkApiGetUsers_validation_json_scheme() {
        //надо положить json в правильную папочку:
//        System.out.println(this.getClass().getResource("/").getPath());

        given ().
                param (PARAM_USER_ID, Integer.parseInt(UserTestingData.getUserID(2))).
                param (PARAM_VERSION, VERSION_VK_API).
        when().
                get(VK_API_GET_USERS_METHOD).
        then().
                assertThat().
                body(matchesJsonSchemaInClasspath("user.json"));
    }

}
